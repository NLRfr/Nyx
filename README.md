# Nyx
> Dévouée à servir la Princess Luna dans la Discorde.

[![PEP8-img]][PEP8]
[![Build-img]][Build]

[Repo] - [Docs]

![](https://gitlab.com/uploads/-/system/project/avatar/3166832/nyx.gif)


## Licence
[MIT]


[PEP8]: https://www.python.org/dev/peps/pep-0008/
[PEP8-img]: https://img.shields.io/badge/code%20style-pep8-green.svg
[Build]: https://gitlab.com/NLRfr/Nyx/-/commits/master
[Build-img]: https://gitlab.com/NLRfr/Nyx/badges/master/pipeline.svg
[Repo]: https://gitlab.com/NLRfr/Nyx
[Docs]: https://nlrfr.gitlab.io/Nyx/
[MIT]: https://nlr.mit-license.org/
