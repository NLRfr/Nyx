import os
import random
import discord
import sqlite3
import configparser
import better_exceptions

from shutil import copyfile
from botutils import is_mod, reply, NyxHelpCommand
from datetime import datetime
from discord.ext import commands
from dateutil.relativedelta import relativedelta

# Setup config
conf = configparser.ConfigParser()
conf.read('./config.ini')

# Setup discord-stuff
intents = discord.Intents.default()
intents.members = True
bot = commands.Bot(
    max_messages=15000,
    command_prefix=commands.when_mentioned_or('!'),
    description="Dévouée à servir la Princess Luna dans la Discorde.",
    help_command=NyxHelpCommand(dm_help=True, no_category="Autres"),
    intents=intents
)

# init
better_exceptions.MAX_LENGTH = None
db = None
sqlite_version = '???'
ignorebin = conf.get('bot', 'ignorebin').split(',')
joinfilter = conf.get('bot', 'joinfilter').split(',')
banlist = []


# On bot login
@bot.event
async def on_ready():
    print('/ -----------------------------------------------------------------------------')
    print('| # ME')
    print('| Name:     ' + bot.user.name)
    print('| ID:       ' + str(bot.user.id))
    print('| Invite:   https://discord.com/oauth2/authorize?client_id=' + str(bot.user.id) +
          '&permissions=397553298678&scope=applications.commands%20bot')
    print('| SQLite:   ' + sqlite_version)
    print('| -----------------------------------------------------------------------------')
    print('| # MODULES')
    # Import our 'modules'
    bot.load_extension('utilitaires')
    bot.load_extension('fun')
    bot.load_extension('mod')
    print('|-----------------------------------------------------------------------------')
    print('| # SERVERS (' + str(len(bot.guilds)) + ')')
    for guild in bot.guilds:
        print('| > Name:   ' + guild.name + ' (' + str(guild.id) + ')')
        if guild.me.nick:
            print('|   Nick:   ' + guild.me.nick)
        print('| > Adding commands...')
        await bot.register_application_commands(guild=guild)
    print('\\-----------------------------------------------------------------------------')


# On new messages
@bot.event
async def on_message(message):
    low_message = message.content.lower()
    mention = message.author.mention

    # Remove ads or too illegal stuff
    no_ads = ['000webhostapp', 'mlp-fan-france.fr', 'dlcorcl-me.com']
    if (any(thing in low_message.replace(' ', '') for thing in no_ads) and
       not is_mod(message.channel, message.author) and
       not str(message.channel.id) in conf.get('bot', 'ignorefilter').split(',')):
        await bot.delete_message(message)
        return

    # Trigger by words and sentences
    quote_42 = ['réponse à la vie, l\'univers et tout le reste', 'answer to life the universe and everything']
    if any(thing in low_message for thing in quote_42):
        await reply(message, '42 <:classyluna:302196645980930049>')

    if 'pain au chocolat' in low_message and 'chocolatine' not in low_message:
        if random.randint(0, 3) > 1:
            await reply(message, '*chocolatine <:chocolatine:305804876968099841>')

    if 'linux' in low_message and 'gnu' not in low_message:
        if random.randint(0, 5) > 3:
            await reply(message, '"I would just like to interject for a moment." Ce que vous semblez ' +
                                 'faire référence à Linux est, en réalité, GNU/Linux ou comme j\'ai commencé à le ' +
                                 'nommer récemment, GNU plus Linux.')

    if 'rt si c trist' in low_message:
        await reply(message, 'rt %s' % mention)

    if 'talk dirty to me' in low_message:
        await reply(message, '_\\*prends sa trompette\\*_ "Tululu tutulu lulu lulululu"')

    if '>>>/' in low_message:
        board = low_message.split('>>>/', 1)[1].split('/', 1)[0]
        sfw_chans = ['3', 'a', 'adv', 'an', 'biz', 'c', 'cgl', 'ck', 'cm', 'co', 'diy', 'fa', 'fit', 'g', 'gd', 'his', 'int', 'jp', 'k',
                     'lgbt', 'lit', 'm', 'mlp', 'mu', 'n', 'news', 'o', 'out', 'p', 'po', 'pw', 'qa', 'qst', 'sci', 'sp', 'tg', 'toy',
                     'trv', 'tv', 'v', 'vg', 'vip', 'vm', 'vmg', 'vp', 'vr', 'vrpg', 'vst', 'vt', 'w', 'wsg', 'wsr', 'x', 'xs']
        nsfw_chans = ['aco', 'b', 'bant', 'd', 'e', 'f', 'gif', 'h', 'hc', 'hm', 'hr', 'i', 'ic', 'pol', 'r', 'r9k', 's', 's4s', 'soc',
                      't', 'trash', 'u', 'wg', 'y']

        if any(thing == board for thing in sfw_chans):
            await reply(message, 'https://boards.4channel.org/%s/' % board)
        elif any(thing == board for thing in nsfw_chans):
            await reply(message, 'https://boards.4chan.org/%s/ **(NSFW)**' % board)

    # Mentionning the bot
    if any(thing.id == bot.user.id for thing in message.mentions):
        hellos = ['salut', 'coucou', 'hello', 'bonjour', 'plop']

        if '?' == low_message[-1:]:
            if random.randint(0, 20) > 16:
                await reply(message, 'non')
            else:
                await reply(message, 'oui')
        elif any(thing in low_message for thing in hellos):
            await reply(message, 'Coucou ' + mention + ' <:NLRblobnomcookie:361111629339033602>')

    # Submit feedback by starting a message with a lightbulb in the appropriate channel
    if str(message.channel.id) in conf.get('bot', 'feedback').split(',') and low_message.startswith('💡'):
        await message.add_reaction('👍')
        await message.add_reaction('👎')
        await message.pin()

    # Log that the user was active
    curr_datetime = datetime.now()
    with db:
        ua_cur = db.cursor()
        ua_cur.execute("INSERT OR REPLACE INTO useractivity(uaUser, uaMsg, uaDate) VALUES (?,?,?)",
                       (str(message.author.id), message.content, curr_datetime))

    # Launch commands if there's any
    await bot.process_commands(message)


# On Interaction (buttons and stuff)
@bot.event
async def on_interaction(interaction):
    # Team roles
    roles = {
        'team_nlr': 970680412449964142,
        'team_se': 970680751819460638,
        'team_ue': 970681154829180968,
        'team_ce': 970681025762062366
    }

    if interaction.custom_id == 'team_clear' or interaction.custom_id in roles:
        for _, v in roles.items():
            r = interaction.user.get_role(v)

            if r is not None:
                await interaction.user.remove_roles(r)

        if interaction.custom_id in roles:
            new_role = interaction.guild.get_role(roles[interaction.custom_id])
            await interaction.user.add_roles(new_role)

        await interaction.response.send_message(
            content='Rôles à jour !',
            ephemeral=True
        )


# On user join
@bot.event
async def on_member_join(member):
    muted = ''

    # Check if user should be filtred
    if any(jf in member.name.lower() for jf in joinfilter):
        await bot.ban(member)  # Ban the user
        return  # Don't post about it

    # Notify in defined channel for the server
    try:
        chan = conf.get(str(member.guild.id), 'modlogs')  # get channel id who gets mod logs
    except configparser.NoOptionError:
        return

    if chan is None:
        return  # If there's nothing, do nothing

    # Adapt to new usernames
    if member.discriminator != '0':
        member_name = member.name + '#' + member.discriminator
    else:
        member_name = '@' + member.name

    # Build an embed
    em = discord.Embed(title=muted + member_name + ' à rejoint',
                       description='Dites bonjour !',
                       colour=0x23D160, timestamp=discord.utils.utcnow())  # color: green
    em.set_footer(text='ID: ' + str(member.id))

    if member.avatar:
        em.set_thumbnail(url=member.avatar.url)

    # Send message with embed
    await bot.get_channel(int(chan)).send('', embed=em)


# On user leave
@bot.event
async def on_member_remove(member):
    # Check if user should be filtred
    if any(jf in member.name.lower() for jf in joinfilter):
        return  # Don't post about it

    # Notify in defined channel for the server
    try:
        chan = conf.get(str(member.guild.id), 'modlogs')  # get channel id who gets mod logs
    except configparser.NoOptionError:
        return

    if chan is None:
        return  # If there's nothing, don't do anything

    # Count for how long an user has been a member
    diff = relativedelta(datetime.utcnow(), member.joined_at.replace(tzinfo=None))
    member_since = member.name + ' était membre depuis '
    if diff.years > 0:
        member_since += str(diff.years) + ' année' + ('s ' if diff.years != 1 else ' ')
    if diff.months > 0:
        member_since += str(diff.months) + ' mois '
    if diff.days > 0:
        member_since += str(diff.days) + ' jour' + ('s ' if diff.days != 1 else ' ')
    if diff.hours > 0:
        member_since += str(diff.hours) + ' heure' + ('s ' if diff.hours != 1 else ' ')
    member_since += str(diff.minutes) + ' minute' + ('s et ' if diff.minutes != 1 else ' et ')
    member_since += str(diff.seconds) + ' seconde' + ('s.' if diff.seconds != 1 else '.')

    # Adapt to new usernames
    if member.discriminator != '0':
        member_name = member.name + '#' + member.discriminator
    else:
        member_name = '@' + member.name

    # Build an embed
    em = discord.Embed(title=member_name + ' a quitté.', description=member_since,
                       colour=0xE81010, timestamp=discord.utils.utcnow())  # color: red
    em.set_footer(text='ID: ' + str(member.id))

    if member.avatar:
        em.set_thumbnail(url=member.avatar.url)

    # Send message with embed
    await bot.get_channel(int(chan)).send('', embed=em)


# On user ban
@bot.event
async def on_member_ban(guild, member):
    # Check if user should be filtred
    if any(jf in member.name.lower() for jf in joinfilter):
        return  # Don't post about it

    # Notify in defined channel for the server
    try:
        chan = conf.get(str(guild.id), 'modlogs')  # get channel id who gets mod logs
    except configparser.NoOptionError:
        return

    if chan is None:
        return  # If there's nothing, don't do anything

    # Build an embed
    em = discord.Embed(title=member.name + ' est banni·e du serveur.',
                       colour=0x7289DA, timestamp=discord.utils.utcnow())  # color: blue
    em.set_footer(text='ID: ' + str(member.id))

    if member.avatar:
        em.set_thumbnail(url=member.avatar.url)

    # Send message with embed
    await bot.get_channel(int(chan)).send('', embed=em)


# On message delete
@bot.event
async def on_message_delete(message):
    # Notify in defined channel for the server
    author = message.author

    try:
        chan = conf.get(str(message.guild.id), 'trashbin')  # get channel id who gets the deleted messages
    except configparser.NoOptionError:
        return

    attch = []
    for a in message.attachments:
        attch.append(a.url)

    print('// Deleted from #' + message.channel.name + ':\n' +
          author.name + ': ' + message.content + ' ' + ' '.join(attch) + '\n')

    if str(message.channel.id) in ignorebin or author.discriminator == '0000':
        return  # Don't do anything if bot or ignored channel

    # Build an embed
    em = discord.Embed(description=message.content + ' ' + ' '.join(attch),
                       colour=0x607D8B, timestamp=message.created_at)  # color: dark grey
    em.set_footer(text='#' + message.channel.name + ' - ID: ' + str(message.id))

    if author.avatar:
        em.set_author(name=author.name, icon_url=author.avatar.url)
    else:
        em.set_author(name=author.name)

    if len(attch) > 0:
        em.set_image(url=attch[0])

    # Send message with embed
    await bot.get_channel(int(chan)).send('', embed=em)


# Connect the bot
async def main():
    await bot.login(conf.get('bot', 'token'))
    await bot.register_application_commands(None)  # Clean commands
    await bot.connect()


# Launch
if __name__ == '__main__':
    try:
        # If there's no database file, copy from the empty one
        if not os.path.isfile('data.db'):
            copyfile('data.db.dist', 'data.db')

        # Connect to SQLite3 DB
        db = sqlite3.connect('data.db')
        with db:
            cur = db.cursor()
            cur.execute('SELECT SQLITE_VERSION()')
            data = cur.fetchone()
            sqlite_version = data[0]
    except Exception as e:
        print("Erreur : " + str(e))
        exit(1)

    try:
        loop = bot.loop
        loop.run_until_complete(main())
    except:
        exit(5)
