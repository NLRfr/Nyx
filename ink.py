import discord
import configparser
import better_exceptions

from botutils import is_mod
from discord.ext import commands

# Setup config
conf = configparser.ConfigParser()
conf.read('./config.ini')

# Setup discord-stuff
description = '"Dévouée à servir la Princess Luna dans la Discorde."'
bot = commands.Bot(command_prefix='!!!!!!', description=description)

# init
better_exceptions.MAX_LENGTH = None


# On bot login
@bot.event
async def on_ready():
    print('/ ----------------- \\')
    print('| Happy inktober! |')
    print('\\ ----------------- /')
    await bot.change_presence(game=discord.Game(name='Inktober'))


# On new messages
@bot.event
async def on_message(message):
    # OSEF si c'est pas notre channel ou si un mod ou s'il y a une image au moins
    if (message.channel.id != 493862776104812545 or is_mod(message) or
       len(message.attachments) > 0 or 'http' in message.content):
        return

    # Sinon poubelle
    print(message.author.name + ': ' + message.content)
    await bot.delete_message(message)


# Launch
if __name__ == '__main__':
    bot.run(conf.get('bot', 'token'))
