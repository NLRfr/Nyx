import configparser
from discord.ext import commands

# Setup config
conf = configparser.ConfigParser()
conf.read('./config.ini')

# Setup discord-stuff
description = '"Dévouée a servir la Princess Luna dans la Discorde."'
bot = commands.Bot(command_prefix='!', description=description)

# Ignore list
ignore = ['Mute', 'Nyx', 'Blue Wave']


# On new messages
@bot.event
async def on_message(message):
    roles = message.guild.roles
    clist = []
    css = 'width:1em;display:inline-block;'
    for r in roles:
        if r.colour.to_tuple() != (0, 0, 0) and r.name not in ignore:  # On a pas de rôles pur-noir, c'est en fait les rôles sans couleurs.
            clist.append({
                'id': r.position,
                'r': r.colour.r,
                'g': r.colour.g,
                'b': r.colour.b,
                'name': r.name
            })

    slist = sorted(clist, key=lambda x: x['id'], reverse=True)

    for clr in slist:
        print('    <span style="background-color:rgb(%i,%i,%i);%s">&nbsp;</span> %s<br>' %
              (clr['r'], clr['g'], clr['b'], css, clr['name']))

    print('\n\n[', end='')

    for i, l in enumerate(slist):
        print('\'%s\', ' % l['name'].lower(), end='')
        if i % 4 == 3:
            print('')

    print("]\n\n")

# Launch
if __name__ == '__main__':
    bot.run(conf.get('bot', 'token'))
