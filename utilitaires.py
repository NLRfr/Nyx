import discord

from botutils import reply
from discord.ext import commands


class Utilitaires(commands.Cog):
    """Commandes pratiques."""
    def __init__(self, bot):
        self.bot = bot
        print('| Loaded:   utilitaires')

    @commands.command(
        application_command_meta=commands.ApplicationCommandMeta(),
        description='Aussi nommé le tenis de table.'
    )
    async def ping(self, ctx):
        """PONG!"""
        try:
            await reply(ctx, 'PONG !', ephemeral=True)
        except Exception as e:
            print('>>> ERROR Ping ', e)

    @commands.command(
        application_command_meta=commands.ApplicationCommandMeta(
            options=[discord.ApplicationCommandOption(
                name="user",
                description="La personne concernée",
                type=discord.ApplicationCommandOptionType.user,
                required=False
            )],
            guild_only=True
        ),
        description='Ajoutez un nom/mention pour quelqu\'un d\'dautre.'
    )
    async def age(self, ctx, *, user: discord.Member = None):
        """Demander quand la personne à rejoint le serveur"""
        author = ctx.author
        # await self.bot.send_typing(ctx.message.channel)

        if user is None:
            user = author

        await reply(ctx, '%s à rejoint le serveur le <t:%d:F>' % (user.name, int(user.joined_at.timestamp())))

    @commands.command(
        application_command_meta=commands.ApplicationCommandMeta(
            options=[discord.ApplicationCommandOption(
                name="choix",
                description="La couleur présente dans la liste des supportées",
                type=discord.ApplicationCommandOptionType.string,
                required=False
            )],
            guild_only=True
        ),
        description='BLEU BLANC ROUGE, C\'EST MOI QUI CHOISI !'
    )
    async def couleur(self, ctx, *, choix: str = ''):
        """Choisir la couleur de votre pseudo"""
        author = ctx.author
        ask = choix.lower()
        color_list = ['purple', 'electric violet', 'cardinal pink', 'rose bonbon',
                      'mona lisa', 'light wisteria', 'blue gem', 'fuchsia blue',
                      'jacksons purple', 'blurple', 'dodger blue', 'half baked', 'sail',
                      'eastern blue', 'anakiwa', 'moss green',
                      'medium sea green', 'tropical rain forest', 'bilbao', 'atlantis',
                      'selective yellow', 'corn', 'drover', 'chartreuse yellow',
                      'carrot orange', 'barley corn', 'brown rust', 'jaffa',
                      'christine', 'red', 'tamarillo', 'gray']

        try:
            if ask in color_list:
                user_roles = author.roles
                ask_role = None

                for r in author.roles:
                    if r.name.lower() in color_list:
                        user_roles.remove(r)

                for r in author.guild.roles:
                    if r.name.lower() == ask:
                        ask_role = r.name
                        user_roles.append(r)

                if ask_role is None:
                    await reply(ctx, 'Ce rôle n\'existe pas/plus, essayez autre chose: ' +
                                     '<https://nlrfr.gitlab.io/Nyx/cmds.html#liste-des-couleurs>',
                                ephemeral=True)
                else:
                    await author.edit(roles=user_roles)
                    await reply(ctx, 'Couleur changée vers %s.' % ask_role)
            else:
                await reply(ctx, 'Merci de préciser une couleur de la liste: ' +
                                 '<https://nlrfr.gitlab.io/Nyx/cmds.html#liste-des-couleurs>',
                            ephemeral=True)
        except Exception as e:
            print('>>> ERROR color', e)


def setup(bot):
    bot.add_cog(Utilitaires(bot))
