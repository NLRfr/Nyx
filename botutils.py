# Helpers
import re
import random
import discord
import requests

from discord.ext.commands import DefaultHelpCommand


def is_admin(channel, author):
    return channel.permissions_for(author).administrator


def is_mod(channel, author):
    return channel.permissions_for(author).kick_members


def random_line_url(url):
    r = requests.get(url)
    lines = r.content
    return random.choice(lines.split(b"\n")).decode('utf-8')


def html_md(format_me):
    # Escape * and _
    format_me = re.sub(r'\*', '\\*', format_me)
    format_me = re.sub(r'_', '\\_', format_me)
    # Bold
    format_me = re.sub(r'<.?b>', '**', format_me)
    # Italic
    format_me = re.sub(r'<.?i>', '_', format_me)
    # Underline
    format_me = re.sub(r'<.?u>', '__', format_me)
    # New lines
    format_me = re.sub(r'<br>', '\n', format_me)
    # Links
    format_me = re.sub(r'<a href="(.*)">(.*)</a>', '[\\2](\\1)', format_me)
    return format_me


async def reply(ctx, message, *, embed: discord.Embed = None, ephemeral: bool = False):
    if isinstance(ctx, discord.ext.commands.context.SlashContext):
        return await ctx.send(message, embed=embed, ephemeral=ephemeral)
    elif isinstance(ctx, discord.Message):
        delete_after = None
        if ephemeral:
            delete_after = 5.0
            await ctx.delete(delay=delete_after)

        return await ctx.reply(message, embed=embed, delete_after=delete_after)
    else:
        delete_after = None
        if ephemeral:
            delete_after = 5.0
            await ctx.message.delete(delay=delete_after)

        return await ctx.reply(message, embed=embed, delete_after=delete_after)


class NyxHelpCommand(DefaultHelpCommand):
    def __init__(self, **options):
        super().__init__(**options)

    def get_ending_note(self):
        return None
