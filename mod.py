import discord

from botutils import is_mod, is_admin, reply
from discord.ext import commands


class Mod(commands.Cog):
    """Commandes de modération."""
    def __init__(self, bot):
        self.bot = bot
        print('| Loaded:   mod')

    @commands.command(
        application_command_meta=commands.ApplicationCommandMeta(
            options=[discord.ApplicationCommandOption(
                name="nbr",
                description="Nombres de messages à supprimer (par défaut: 50)",
                type=discord.ApplicationCommandOptionType.integer,
                required=False
            )],
            guild_only=True
        ),
        description='Spécifiez un nombre ou je supprime les 50 derniers messages.'
    )
    async def rm(self, ctx, nbr: int = 50):
        """[MOD] Supprimer des messages"""
        if not is_mod(ctx.channel, ctx.author):
            await reply(ctx, 'Désolée, mais vous ne pouvez pas faire ça.', ephemeral=True)
            return

        try:
            # await self.bot.send_typing(ctx.message.channel)
            deleted = await ctx.channel.purge(before=ctx.message, limit=int(nbr))
            await reply(ctx, '%d messages supprimés.' % len(deleted), ephemeral=True)
        except Exception as e:
            print('>>> ERROR rm ', e)

    @commands.command(
        application_command_meta=commands.ApplicationCommandMeta(
            options=[discord.ApplicationCommandOption(
                name="user",
                description="La personne concernée",
                type=discord.ApplicationCommandOptionType.user,
                required=True
            )],
            guild_only=True
        ),
        description='TO THE MOOOOOON!'
    )
    async def moon(self, ctx, *, user: discord.Member = None):
        """[MOD] Kick une personne sur la lune"""
        if not is_mod(ctx.channel, ctx.author):
            await reply(ctx, 'Désolée, mais vous ne pouvez pas faire ça.', ephemeral=True)
            return

        if user is None:
            await reply(ctx, 'Il me faut un nom pour envoyer quelqu\'un sur la lune !', ephemeral=True)
        elif user.id == self.bot.user.id:
            await reply(ctx, 'Hey !')
        else:
            await reply(ctx, '_\\*Prends **%s** et vise la lune\\*_ TO THE MOOOOOON! <:sillyluna:302196649470459904>' % user.name)
            await ctx.guild.kick(user)

    @commands.command(
        application_command_meta=commands.ApplicationCommandMeta(
            options=[discord.ApplicationCommandOption(
                name="user",
                description="La personne concernée",
                type=discord.ApplicationCommandOptionType.user,
                required=True
            )],
            guild_only=True
        ),
        description='Zou, to the Sun!'
    )
    async def sun(self, ctx, *, user: discord.Member = None):
        """[MOD] Bannir une personne sur le soleil"""
        if not is_mod(ctx.channel, ctx.author):
            await reply(ctx, 'Désolée, mais vous ne pouvez pas faire ça.', ephemeral=True)
            return

        if user is None:
            await reply(ctx, 'Il me faut un nom pour envoyer quelqu\'un sur le soleil !', ephemeral=True)
        elif user.id == self.bot.user.id:
            await reply(ctx, 'Hey !')
        else:
            await reply(ctx, '_\\*Prends **%s** et vise le soleil\\*_ Burn baby, **BURN!** <:srsly:302201014952394754>' % user.name)
            await ctx.guild.ban(user)

    @commands.command(
        application_command_meta=commands.ApplicationCommandMeta(
            guild_only=True
        ),
        description='Je reviendrai !'
    )
    async def reboot(self, ctx):
        """[ADMIN] Redémarrer le bot."""
        if not is_admin(ctx.channel, ctx.author):
            await reply(ctx, 'Désolée, mais vous ne pouvez pas faire ça.', ephemeral=True)
            return

        await reply(ctx, 'OK, je re-', ephemeral=True)
        exit(0)

    @commands.command(
        description='Afficher le message de choix d\'équipe'
    )
    async def teamSelect(self, ctx):
        """[ADMIN] Choix d'une équipe."""
        if not is_admin(ctx.channel, ctx.author):
            await reply(ctx, 'Désolée, mais vous ne pouvez pas faire ça.', ephemeral=True)
            return

        components = discord.ui.MessageComponents(
            discord.ui.ActionRow(
                discord.ui.Button(
                    custom_id='team_nlr',
                    style=discord.ButtonStyle.primary,
                    label="Pour Luna",
                    emoji=discord.PartialEmoji(
                        name='NLR',
                        id=302199670518579200
                    )
                ),
                discord.ui.Button(
                    custom_id='team_se',
                    style=discord.ButtonStyle.primary,
                    label="Pour Celestia",
                    emoji=discord.PartialEmoji(
                        name='SolarEmpire',
                        id=970679278888955974
                    )
                ),
                discord.ui.Button(
                    custom_id='team_ue',
                    style=discord.ButtonStyle.primary,
                    label="Pour l'Union",
                    emoji=discord.PartialEmoji(
                        name='UnitedEquestria',
                        id=970679441439215696
                    )
                ),
                discord.ui.Button(
                    custom_id='team_ce',
                    style=discord.ButtonStyle.primary,
                    label="Pour Discord",
                    emoji=discord.PartialEmoji(
                        name='ChaosEmpire',
                        id=970680376236343386
                    )
                ),
                discord.ui.Button(
                    custom_id='team_clear',
                    style=discord.ButtonStyle.danger,
                    label="Retirer les rôles",
                    emoji=discord.PartialEmoji(name='❎')
                )
            )
        )
        await ctx.send('Choisissez votre camp...', components=components)


def setup(bot):
    bot.add_cog(Mod(bot))
