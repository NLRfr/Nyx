Commandes publiques
===================

.. toctree::
   :maxdepth: 2

- ``/cena`` : ???
- ``/derpi`` : Retourne une image au hasard depuis Derpibooru.
- ``/dé`` : Vous permet de tirer un dé ! Vous pouvez préciser le nombre de faces, ex : ``/dé 12``.
- ``/fr`` : Sortir le Bescherelle.
- ``/nope`` : Publie une image "nope" au hasard.
- ``/ok`` : Publie une image "ok." au hasard.
- ``/ping`` : PONG ! Permet de vérifier que le bot est attentif.
- ``/quote`` : Cite un passage de la série.
- ``/couleur [couleur]`` : Choisir la couleur de votre pseudo, ex : ``/couleur Blurple``. Liste ci-dessous.


Liste des couleurs
------------------

.. raw:: html

    <span style="background-color:rgb(126,0,189);width:1em;display:inline-block;">&nbsp;</span> Purple<br>
    <span style="background-color:rgb(157,0,255);width:1em;display:inline-block;">&nbsp;</span> Electric Violet<br>
    <span style="background-color:rgb(143,10,107);width:1em;display:inline-block;">&nbsp;</span> Cardinal Pink<br>
    <span style="background-color:rgb(249,66,158);width:1em;display:inline-block;">&nbsp;</span> Rose Bonbon<br>
    <span style="background-color:rgb(255,153,153);width:1em;display:inline-block;">&nbsp;</span> Mona Lisa<br>
    <span style="background-color:rgb(209,171,217);width:1em;display:inline-block;">&nbsp;</span> Light Wisteria<br>
    <span style="background-color:rgb(84,14,158);width:1em;display:inline-block;">&nbsp;</span> Blue Gem<br>
    <span style="background-color:rgb(106,84,194);width:1em;display:inline-block;">&nbsp;</span> Fuchsia Blue<br>
    <span style="background-color:rgb(27,54,143);width:1em;display:inline-block;">&nbsp;</span> Jacksons Purple<br>
    <span style="background-color:rgb(114,137,218);width:1em;display:inline-block;">&nbsp;</span> Blurple<br>
    <span style="background-color:rgb(33,150,243);width:1em;display:inline-block;">&nbsp;</span> Dodger Blue<br>
    <span style="background-color:rgb(130,178,206);width:1em;display:inline-block;">&nbsp;</span> Half Baked<br>
    <span style="background-color:rgb(184,215,251);width:1em;display:inline-block;">&nbsp;</span> Sail<br>
    <span style="background-color:rgb(22,138,175);width:1em;display:inline-block;">&nbsp;</span> Eastern Blue<br>
    <span style="background-color:rgb(168,254,255);width:1em;display:inline-block;">&nbsp;</span> Anakiwa<br>
    <span style="background-color:rgb(174,217,177);width:1em;display:inline-block;">&nbsp;</span> Moss Green<br>
    <span style="background-color:rgb(36,186,101);width:1em;display:inline-block;">&nbsp;</span> Medium Sea Green<br>
    <span style="background-color:rgb(0,120,80);width:1em;display:inline-block;">&nbsp;</span> Tropical Rain Forest<br>
    <span style="background-color:rgb(31,120,16);width:1em;display:inline-block;">&nbsp;</span> Bilbao<br>
    <span style="background-color:rgb(182,205,48);width:1em;display:inline-block;">&nbsp;</span> Atlantis<br>
    <span style="background-color:rgb(255,186,0);width:1em;display:inline-block;">&nbsp;</span> Selective Yellow<br>
    <span style="background-color:rgb(228,180,0);width:1em;display:inline-block;">&nbsp;</span> Corn<br>
    <span style="background-color:rgb(252,245,165);width:1em;display:inline-block;">&nbsp;</span> Drover<br>
    <span style="background-color:rgb(221,255,0);width:1em;display:inline-block;">&nbsp;</span> Chartreuse Yellow<br>
    <span style="background-color:rgb(242,138,19);width:1em;display:inline-block;">&nbsp;</span> Carrot Orange<br>
    <span style="background-color:rgb(159,137,88);width:1em;display:inline-block;">&nbsp;</span> Barley Corn<br>
    <span style="background-color:rgb(167,112,59);width:1em;display:inline-block;">&nbsp;</span> Brown Rust<br>
    <span style="background-color:rgb(235,111,61);width:1em;display:inline-block;">&nbsp;</span> Jaffa<br>
    <span style="background-color:rgb(242,89,12);width:1em;display:inline-block;">&nbsp;</span> Christine<br>
    <span style="background-color:rgb(255,0,0);width:1em;display:inline-block;">&nbsp;</span> Red<br>
    <span style="background-color:rgb(140,21,21);width:1em;display:inline-block;">&nbsp;</span> Tamarillo<br>
    <span style="background-color:rgb(126,126,126);width:1em;display:inline-block;">&nbsp;</span> Gray<br>
