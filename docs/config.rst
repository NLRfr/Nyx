Configuration
=============

.. toctree::
   :maxdepth: 2


Obtenir un token depuis Discord
-------------------------------

- Rendez-vous dans la partie `Applications`_ de Discord.
- Créez une nouvelle application. (Le nom et l'icône suffisent.)
- Cliquez sur "Bot" dans le menu à gauche, puis "Add Bot".
- Cliquez sur le bouton "Reset Token" et gardez le code de côté.


Fichier de configuration
------------------------

Section ``[bot]``
*****************

- ``token`` : Token du bot.
- ``derpikey`` : Clé API pour Derpibooru.
- ``ignorebin`` : Liste d'IDs des salons à ignorer pour la poubelle (séparés par des virgules sans espaces).
- ``ignorefilter`` : Liste d'IDs des salons où le filtre ne s'activera pas (filtre comme l'anti-lien vers d'autres serveurs).
- ``feedback`` : Liste d'IDs des salons où sont postés les feedbacks.

Sections ``[ID DU SERVEUR ICI]``
********************************

- ``modlogs`` : ID du salon qui aura les logs des gens qui rejoignent, quittent ou se font bannir du serveur.
- ``trashbin`` : ID du salon qui aura les message supprimés.
- ``admin`` : ID du rôle des admins.
- ``mod`` : ID du rôle des modérateurs.
- ``mute`` : ID du rôle des mute.


.. _Applications: https://discord.com/developers/applications
