Documentation pour Nyx
======================

**Code source**: https://gitlab.com/NLRfr/Nyx

.. toctree::
   :maxdepth: 2

   install
   config
   cmds
   cmds-admin


