Commandes pour les administrateurs
==================================

.. toctree::
   :maxdepth: 2

- ``/moon [personne]`` : Kick une personne sur la lune.
- ``/reboot`` : Redémarrer le bot. (S'il n'est pas down évidemment.)
- ``/rm [nombre]`` : Supprime un certain nombre de messages. Par défaut, 50 messages.
- ``/sun [personne]`` : Bannir une personne sur le soleil.
