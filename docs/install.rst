Installation
============

.. toctree::
   :maxdepth: 2


Pré-requis
----------

- `Python`_ 3.10 ou supérieur.
- Un compte et un serveur sur `Discord`_.


Récupération du code
--------------------

::

    git clone https://gitlab.com/NLRfr/Nyx.git
    cd Nyx
    cp config.ini.dist config.ini


Utilisation de ``pyenv``
------------------------

Vous pouvez installer ``pyenv`` avec `pyenv-installer`_ pour vous simplifier la vie.  
Cette étape n'est pas obligatoire si vous voulez y aller manuellement.

::

    # Installer l'utilitaire
    curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash

    # Dans le dossier de Nyx, installer la version recommandée
    pyenv install

    # Pour forcer le passage à une version
    pyenv version # Lister les versions disponibles
    pyenv shell <version> # ex: 3.10.4

    # Vérifier la version
    python --version


Dépendances python
------------------

Une fois que vous êtes sur une version de Python compatible...

::

    # Ajouter le sudo si vous n'utilisez pas pyenv/virtualenv
    pip install -r requirements.txt


Lancement
---------

- `Configurez le bot en modifiant son fichier de configuration, <config.html>`_
- Dans un tmux/screen/watcher: ``python nyx.py``
- Invitez le bot dans votre serveur en ouvrant le lien qu'il s'affiche lors de son démarrage.


.. _Python: https://www.python.org/
.. _Discord: https://discord.com/
.. _pyenv-installer: https://github.com/pyenv/pyenv-installer
