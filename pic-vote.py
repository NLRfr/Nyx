import configparser
import better_exceptions

from discord.ext import commands

# Setup config
conf = configparser.ConfigParser()
conf.read('./config.ini')

# Setup discord-stuff
description = '"Dévouée a servir la Princess Luna dans la Discorde."'
bot = commands.Bot(command_prefix='!!!!!!', description=description)

# init
better_exceptions.MAX_LENGTH = None


# On bot login
@bot.event
async def on_ready():
    print('Je suis prête !')


# On new messages
@bot.event
async def on_message(message):
    # OSEF si c'est pas notre channel
    if str(message.channel.id) != '398504525436157962':
        return

    domains = ['deviantart.com', 'deviantart.net', 'derpi', 'tumblr.']

    if len(message.attachments) > 0 or any(dom in message.content for dom in domains):
        await message.add_reaction('👍')
        await message.add_reaction('👎')

    if len(message.attachments) > 0 and 'http' not in message.content:
        await message.add_reaction('🚫')
        await message.channel.send('Je ne trouve pas de lien, pensez à ajouter la source ou votre image sera exclue !')


# Launch
if __name__ == '__main__':
    bot.run(conf.get('bot', 'token'))
